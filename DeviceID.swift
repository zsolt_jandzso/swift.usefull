//
//  DeviceID.swift
//  Swift.UseFull
//
//  Created by Jandzso Zsolt on 2016. 10. 27..
//  Copyright © 2016. Jandzso Zsolt. All rights reserved.
//

import UIKit

let deviceID = DeviceID()

class DeviceID: NSObject {

    func GetUniqueDeviceID() -> String
    {        
       
        return UIDevice.current.identifierForVendor!.uuidString
 
    }

}
