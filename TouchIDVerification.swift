//
//  TouchIDVerification.swift
//  Swift.UseFull
//
//  Created by Jandzso Zsolt on 2016. 10. 26..
//  Copyright © 2016. Jandzso Zsolt. All rights reserved.
//

import UIKit
import LocalAuthentication

class TouchIDVerification: NSObject {

    
    //dont forget to add LocalAuthentication.framework to the project
    
    
    enum AuthenticateResult
    {
        case Success
        case WrongTouchID
        case KnownError
        case NotSupported
        case UnknownError
    }
    
    
    
    func AuthenticateWithTouchID() -> AuthenticateResult
    {
        let authenticationContext = LAContext()
        
        
        var error:NSError?
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            
            print("No Biometric Sensor Has Been Detected. This device does not support Touch Id.")
            return AuthenticateResult.NotSupported
        }
        
        var authenticateResult : AuthenticateResult
        
        authenticateResult = AuthenticateResult.UnknownError
        
        authenticationContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Only device owner is allowed here", reply:
            {
                (success, error) -> Void in
            
                if( success )
                {
                
                    print("Fingerprint recognized. You are a device owner!")
                
                    authenticateResult = AuthenticateResult.Success
                
            
                } else
                {
                
                    // Check if there is an error
                
                    if let errorObj = error {
                    
                        print("Error took place. \(errorObj.localizedDescription)")
                    
                        authenticateResult = AuthenticateResult.KnownError
                }
                
                authenticateResult = AuthenticateResult.UnknownError
            }
        })
        
        return authenticateResult
    }
}
