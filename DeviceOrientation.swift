//
//  UseFull.swift
//  Swift.UseFull
//
//  Created by Jandzso Zsolt on 2016. 10. 26..
//  Copyright © 2016. Jandzso Zsolt. All rights reserved.
//

import UIKit

class UseFull: NSObject {
    
    func determineMyDeviceOrientation()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Device is in landscape mode")
        } else {
            print("Device is in portrait mode")
        }
    }
}
