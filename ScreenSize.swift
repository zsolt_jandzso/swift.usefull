
//
//  ScreenSize.swift
//  Swift.UseFull
//
//  Created by Jandzso Zsolt on 2016. 10. 26..
//  Copyright © 2016. Jandzso Zsolt. All rights reserved.
//

import UIKit

class ScreenSize: NSObject {

    func GetScreenSize(){
    
        // Get main screen bounds
        let screenSize: CGRect = UIScreen.main.bounds
        
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        print("Screen width = \(screenWidth), screen height = \(screenHeight)")
    
    }
    
}
